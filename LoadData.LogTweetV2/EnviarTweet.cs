﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Timers;
using System.Configuration;
using LoadData.DataSourse;
using System.Linq;
using Twitterizer;

public static class EnviarTweet
{
	public static bool EnviarPelicula(ref EventLog eventLog)
	{
		dbPeliculasDataContext _db = new dbPeliculasDataContext();
		
		if (_db != null)
		{
			var _Pelicula = _db.Peliculas.Where(n => n.Tweeted == false && n.PostedId != null && n.PostedId > 0).FirstOrDefault();

			if (_Pelicula != null)
			{
				
				string _UrlPelicula = ConfigurationManager.AppSettings.Get("UrlPelicula");
				ProcesoEnvio(_Pelicula.Nombre, _UrlPelicula + _Pelicula.PostedId.ToString(), ref eventLog);
				_Pelicula.Tweeted = true;
				_db.SubmitChanges();
				saveError("Corrida Con Exito!", false);
				return true;
				
			}
			else
			{
				string errorMensaje = "No Existe Pelicula con el Criterio de Tweeted en False y con PostedId mayor que cero.";
				if (eventLog != null)
				{
					eventLog.WriteEntry(errorMensaje, EventLogEntryType.Error, 2);
				}
				else
				{
					saveError(errorMensaje);
				}
			}
		}
		else
		{
			string errorMensaje = "No puede conectarse a la base de datos..";
			if (eventLog != null)
			{
				eventLog.WriteEntry(errorMensaje, EventLogEntryType.Error, 2);
			}
			else
			{
				saveError(errorMensaje);
			}

		}

		return false;
		
	}

	private static void ProcesoEnvio(string nombrePelicula, string linkDescarga, ref EventLog eventLog)
	{
		
		string personasMencionadas = "@GlesannyPeralta @40principalesRD @MalaquiasAngulo";

		bool istest = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("isTest"));
		// true
		
		string mensaje1 = ConfigurationManager.AppSettings.Get("mensaje1");
		// "{0}. la puedes descargar en: {1} {2} gratis como archivo torrents."
		
		string hastag0 = ConfigurationManager.AppSettings.Get("hasTag0");
		// ""
		
		string mensaje0_enviar = string.Format(mensaje1, nombrePelicula, linkDescarga, hastag0);
		
		TweetLog nuevo_item = new TweetLog();
		
		if (istest)
		{
			nuevo_item.isError = false;
			nuevo_item.ErrorMessage = "Test";
			nuevo_item.Contenido = mensaje0_enviar;
		}
		else
		{
			var tweetResponse = EnviarEstadoTweet(mensaje0_enviar);

			if (tweetResponse.Result == RequestResult.Success)
			{
				nuevo_item.isError = false;
				nuevo_item.ErrorMessage = tweetResponse.Content;
				nuevo_item.Contenido = mensaje0_enviar;
			}
			else
			{
				nuevo_item.isError = true;
				nuevo_item.ErrorMessage = tweetResponse.Content;
				nuevo_item.Contenido = tweetResponse.ErrorMessage;
			}
			nuevo_item.ErrorMessage = tweetResponse.Content;
			nuevo_item.Contenido = mensaje0_enviar;
		}

		nuevo_item.fecha = DateTime.Now;
		addToDataBase(nuevo_item);

	}
	public static TwitterResponse<TwitterStatus> EnviarEstadoTweet(string Mensaje_Enviar)
	{
		TwitterResponse<TwitterStatus> retVal = null;

		OAuthTokens tokens = new OAuthTokens();
		tokens.ConsumerKey = ConfigurationManager.AppSettings.Get("ConsumerKey");
		// "7nzErcJSQDxjW2HSMfu5aBFs7"
		tokens.ConsumerSecret = ConfigurationManager.AppSettings.Get("ConsumerSecret");
		// "QZCBF7LyJJebCiMr2s9OcNDiMst3SU7QpRh8CYPXV53gB9IwQj"
		tokens.AccessToken = ConfigurationManager.AppSettings.Get("AccessToken");
		// "2253581773-iPsRYWW8h5GQ5Okn6dBd0EqORpIW2vkOlyPE0wr"
		tokens.AccessTokenSecret = ConfigurationManager.AppSettings.Get("AccessTokenSecret");
		// "QtC7wyZG1MN7KqOBY1SyB0N8KDyRSEt6HDo75JA1VuVEZ"

		StatusUpdateOptions userOptions = new StatusUpdateOptions();
		userOptions.APIBaseAddress = ConfigurationManager.AppSettings.Get("APIBaseAddress");

		userOptions.UseSSL = true;
		// // <-- needed for API 1.1

		retVal = TwitterStatus.Update(tokens, Mensaje_Enviar, userOptions);

		return retVal;
	}
	private static void addToDataBase(TweetLog item)
	{
		dbPeliculasDataContext db = new dbPeliculasDataContext();
		db.TweetLogs.InsertOnSubmit(item);
		db.SubmitChanges();
	}
	private static void saveError(string errorMensaje, bool isError = true) {
		TweetLog nuevo_item = new TweetLog();
		nuevo_item.ErrorMessage = errorMensaje;
		nuevo_item.isError = isError;
		nuevo_item.fecha = DateTime.Now;
		addToDataBase(nuevo_item);
	}
}
