﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Data;
using System.Diagnostics;
using System.Configuration;

namespace LoadData.Infrastructure
{

	public abstract class clProgramaConsola : IclProgramaConsola
	{

		#region "Variables"
		public System.Timers.Timer _timerUpload { get; set; }
		public System.Threading.Thread _trdUploadNotificacion { get; set; }
		public int _TiempoDeNotificacionUpload
		{
			get { return Convert.ToInt32(ConfigurationManager.AppSettings.Get("TiempoDeNotificacion")) * 1000; }
		}
		#endregion

		#region "Eventos"

		public void _timerUpload_Tick()
		{
			if (_trdUploadNotificacion == null || !_trdUploadNotificacion.IsAlive)
			{
				_trdUploadNotificacion = new System.Threading.Thread(this._timerUpload_Tick_Hijo);
				_trdUploadNotificacion.IsBackground = true;
				_trdUploadNotificacion.Start();
			}
		}

		public void _timerUpload_Tick_Hijo()
		{

			string strOrdenesNoProcesadas = this.BeginUpload();

			if (!string.IsNullOrEmpty(strOrdenesNoProcesadas.ToString()))
			{
				if (strOrdenesNoProcesadas.Length > 2000)
				{
					strOrdenesNoProcesadas = strOrdenesNoProcesadas.Substring(0, 2000);
				}
			}

		}

		public abstract string BeginUpload();

		#endregion

	}

}
