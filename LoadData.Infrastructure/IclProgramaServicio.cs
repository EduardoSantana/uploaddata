﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Data;
using System.Diagnostics;

namespace LoadData.Infrastructure
{
	public interface IclProgramaServicio 
	{
		#region "Variables"
		EventLog _EventLog1 { get; set; }
		System.Timers.Timer _timerUpload { get; set; }
		System.Threading.Thread _trdUploadNotificacion { get; set; }
		#endregion
		int _TiempoDeNotificacionUpload { get; }
		#region "Eventos"
		void _timerUpload_Tick(ref EventLog even);
		void _timerUpload_Tick_Hijo();
		string BeginUpload();
		#endregion

	}

}
