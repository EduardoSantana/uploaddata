﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LoadData.ScreenCapture
{
	public class WindowsCaptureScreen
	{
		public static void Capture()
		{

			Bitmap screenshot = new Bitmap(SystemInformation.VirtualScreen.Width,
							   SystemInformation.VirtualScreen.Height,
							   System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			Graphics screenGraph = Graphics.FromImage(screenshot);
			screenGraph.CopyFromScreen(SystemInformation.VirtualScreen.X,
									   SystemInformation.VirtualScreen.Y,
									   0,
									   0,
									   SystemInformation.VirtualScreen.Size,
									   CopyPixelOperation.SourceCopy);
			screenshot.Save(getFullPathAndName(), System.Drawing.Imaging.ImageFormat.Jpeg);
		}
		
		public static string getFullPathAndName(){
			string imagesFolderDate = DateTime.Now.ToString("yyyy-MM-dd") + @"\";
			string path = getFullPath() + imagesFolderDate;
			if (!System.IO.Directory.Exists(path))
			{
				System.IO.Directory.CreateDirectory(path);
			}
			//return path + DateTime.Now.ToString("yyyy-MM-dd_HH-mm_") + DateTime.Now.Ticks.ToString() + ".jpg";
			return path + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-ffff") + ".jpg";
		}


		public static string getFullPath()
		{
			//string systemRoot = Environment.GetEnvironmentVariable("SystemRoot");
			//string windir = Environment.GetEnvironmentVariable("windir");
			string userProfile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
			string imagesFolder = @"\AppData\Local\Microsoft\TimedPodCast\Images\";
			return userProfile + imagesFolder;
		}

	}
}
