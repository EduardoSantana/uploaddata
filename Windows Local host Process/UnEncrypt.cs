﻿using LoadData.Encrypt64;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Windows_Local_host_Process
{
    class UnEncrypt
    {

        private void SetStringToFile(string FILENAME, string message, bool append = false)
        {
            var streamWriter = new StreamWriter(FILENAME, append);
            streamWriter.WriteLine(message);
            streamWriter.Close();
        }

        private string GetStringFromFile(string FILENAME)
        {
            if (File.Exists(FILENAME))
            {
                var fileStream = new FileStream(FILENAME, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    var retV = streamReader.ReadToEnd();
                    return Encrypt.DecryptB64(retV);
                }
            }
            else
            {
                return "";
            }
        }

        public bool CreateDecrytedFile(string FILENAME)
        {
            // read the file with filePath + fileName
            string fileContent = GetStringFromFile(FILENAME);

            // save the decrypted text to new file named filename_descripted.txt
            SetStringToFile(FILENAME.Replace(".log", "") + "_decrypted.txt", fileContent);

            return true;
        }

        public void Execute()
        {
            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory());

            foreach (var item in filePaths)
            {
                if (item.Contains(".log") && !item.Contains("decrypted")) // This contains .log and not contains decrypted.
                {
                    CreateDecrytedFile(item);
                }
            }

        }
    }
}
