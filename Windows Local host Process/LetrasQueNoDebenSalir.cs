﻿using LoadData.ScreenCapture;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Windows_Local_host_Process;

namespace LoadData.KeyCapture
{
	public class LetrasQueNoDebenSalir
	{
		private static GenericRepositoryFiles<KeyCode> repositoryFiles = new GenericRepositoryFiles<KeyCode>(false, true, StaticDataStore.KeyCodes);
		
		private static List<KeyCode> lista
		{
			get
			{
                return repositoryFiles.List.ToList();
            }
		}

		public static void ConvertToXML()
		{
			foreach (var item in lista)
			{
				repositoryFiles.Add(item);
			}
            var p2 = 0;
        }

        public static void TestList()
        {
            var p1 = lista;
            var p2 = 0;
        }

		public static string GetCaracter(int vkCode)
		{

			if (vkCode == 13)
				return Environment.NewLine;

			var obj = lista.Where(p => p.ValorInt == vkCode).FirstOrDefault();
			if (obj != null)
			{
				if (obj.Imprimir)
				{
					if (!string.IsNullOrEmpty(obj.Representa))
						return obj.Representa;
					return " {" + ((System.Windows.Forms.Keys)vkCode).ToString() + "-" + vkCode.ToString() + "} ";
				}
				else
				{
					return "";
				}
			}

			return ((System.Windows.Forms.Keys)vkCode).ToString();
		}
		
	}
	
}
