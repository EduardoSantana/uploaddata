﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadData.ScreenCapture
{ 
    public abstract class ItemRepositoryBase
    {
        public int Id { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
