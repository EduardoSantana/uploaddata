﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Data;
using System.Diagnostics;

namespace LoadData.Infrastructure
{
	public interface IclProgramaConsola
	{
		#region "Variables"
		System.Timers.Timer _timerUpload { get; set; }
		System.Threading.Thread _trdUploadNotificacion { get; set; }
		#endregion
		int _TiempoDeNotificacionUpload { get; }
		#region "Eventos"
		void _timerUpload_Tick();
		void _timerUpload_Tick_Hijo();
		string BeginUpload();
		#endregion
	}

}
