﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoadData.UploadFtp
{
    public class ConfigData
    {
        public ConfigData(bool loadDefault)
        {
            if (loadDefault)
            {
                RutaCarpetaRemota = ConfigurationManager.AppSettings["RutaCarpetaRemota"];
                ServidorFTP = ConfigurationManager.AppSettings["ServidorFTP"];
                PuertoFTP = ConfigurationManager.AppSettings["PuertoFTP"];
                LoginAFP = ConfigurationManager.AppSettings["LoginAFP"];
                Password = ConfigurationManager.AppSettings["Password"];
            }
        }

        public string RutaCarpetaRemota { get; set; }
        public string ServidorFTP { get; set; }
        public string PuertoFTP { get; set; }
        public string LoginAFP { get; set; }
        public string Password { get; set; }
    }
}
