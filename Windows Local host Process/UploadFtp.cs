﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilities.FTP;
using System.Configuration;
using System.IO;

namespace LoadData.UploadFtp
{
	public static class UploadFtp
	{
		public static bool UploadFile(string rutaArchivo, string nombreArchivo)
		{
            var config = new ConfigData(true);
			FTPclient ftp = new FTPclient(config.ServidorFTP + ":" + config.PuertoFTP, config.LoginAFP, config.Password);
			return ftp.Upload(rutaArchivo + nombreArchivo, config.RutaCarpetaRemota + nombreArchivo);
		}

        public static bool UploadFile(string filePath, ConfigData config)
        {
            string imagesFolderDate = DateTime.Now.ToString("yyyy-MM-dd");

            var userName = Environment
                .UserName
                .Replace(" ", "")
                .Replace("?", "")
                .Replace("/", "")
                .Replace("\\", "");
            
            FTPclient ftp = new FTPclient(config.ServidorFTP + ":" + config.PuertoFTP, config.LoginAFP, config.Password);

            bool userDirExist = false;
            foreach (var item in ftp.ListDirectory(config.RutaCarpetaRemota))
            {
                if (item.Replace(config.RutaCarpetaRemota, "") == userName)
                {
                    userDirExist = true;
                }
            }

            if (!userDirExist)
            {
                ftp.FtpCreateDirectory(config.RutaCarpetaRemota + userName);
            }

            bool userDirDateExist = false;
            foreach (var item in ftp.ListDirectory(config.RutaCarpetaRemota + userName))
            {
                if (item.Replace(config.RutaCarpetaRemota + userName, "") == imagesFolderDate)
                {
                    userDirDateExist = true;
                }
            }

            if (!userDirDateExist)
            {
                ftp.FtpCreateDirectory(config.RutaCarpetaRemota + userName + "/" + imagesFolderDate);
            }

            string finalFolder = config.RutaCarpetaRemota + userName + "/" + imagesFolderDate + "/";

            return ftp.Upload(filePath, finalFolder + Path.GetFileName(filePath));
        }
    }
}
