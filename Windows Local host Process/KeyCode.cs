﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadData.ScreenCapture
{
    public class KeyCode : ItemRepositoryBase
    {
        public string Nombre { get; set; }
        public string Description { get; set; }
        public int ValorInt { get; set; }
        public bool Imprimir { get; set; }
        public string Representa { get; set; }
    }
}
