﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyLoan.CorreosManager
{
	public class MailsManager
	{
		private static List<CorreosEnviar> _correos { get; set; }
		private static List<CorreosEnviar> correos
		{
			get
			{
				if (_correos == null)
					_correos = new List<CorreosEnviar>();
				_correos = _correos.Where(p => p.Enviado == false).ToList();
				return _correos;
			}
		}
		public static void Enviar() {
			foreach (CorreosEnviar item in correos)
			{
				SMTPMailHelper.SendMail(item.From, item.To, item.Subject, item.Body);
			}
		}
		public static void Remover(CorreosEnviar item)
		{
			_correos.Remove(item);
		}
		public static void Agregar(CorreosEnviar item)
		{
			_correos.Add(item);
		}
	}

	

}
