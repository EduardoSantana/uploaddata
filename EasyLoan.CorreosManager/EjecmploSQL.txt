﻿Create Table Bitacoras (
  BitacoraId int identity primary key,
  EventoId int not null,
  Comentario varchar(2000),
  UsuarioId varchar(250)
);
Alter table Bitacoras Add CorreoId int null
Create Table Eventos (
  EventoId int identity primary key,
  Nombre varchar(500),
  Descripcion varchar(500)
);
Insert Into Eventos (Nombre, Descripcion) Values ('Notificacion Correo', 'Notificacion Correo');

Create Table CorreosEnviar(
	CorreoId int primary identity key,
	[From] varchar(500),
	[To] varchar(2000),
	[Subject] varchar(500),
	Body varchar(max),
	UsuarioId varchar(500),
	AppId int,
	Enviado bit not null, 
	FechaEnviado DateTime, 
	Error bit, 
	DetalleError varchar(max),
	FechaError DateTime,
	Reintentos int	
)
Alter Table CorreosEnviar Add FechaEnviar DateTime 
Alter Table CorreosEnviar Add Prioridad int not null
Alter Table CorreosEnviar Add FechaCreacion DateTime Not null Default (getDate());

Si tiene la fecha de visita menos a la fecha actual, entonces enviar un correo de ese cliente agregando los datos del cliente y del pretamo.
Vemos el ultimo 
--Alter Table Amortizacion Add Notificado1 bit;
--Alter Table Prestamo Add Notificar1 bit;
--Alter Table Cliente Add Notificar1 bit;

Declare @NombreCliente as Varchar(80)  = '';
Declare @NombrePrestamdor as Varchar(80) = '';
Declare @MontoAdeudado as Varchar(30) = '50,000.00';
Declare @MontoDesembolsado as Varchar(30) = '30,000.00';
Declare @FechaDesembolsado as Varchar(30) = '2017/01/01';
Declare @PrestamoNo as int = 0;
DECLARE qryPrestamos CURSOR FOR 
Select 
	c.Nombre, 
	u.cci_Nombre,
	p.Monto_Pendiente,
	p.Monto_Desembolso,
	CONVERT (varchar(30), p.Fecha_Desembolso , 111),
	p.Prestamo_No
From Prestamo p
Inner join Cliente c on p.Cliente_No = c.Cliente_No
Inner join aspnet_Users u on p.CreadoPor = u.UserName
Where p.Notificar1 = 1 and c.Notificar1 = 1 and Exists(
	Select Top 1 amo.Prestamo_No 
	From Amortizacion amo
	Where p.Prestamo_No = amo.Prestamo_No and amo.Pagado = 0 and amo.Fecha_Pago < GETDATE() and isnull(amo.Notificado1, 0) = 0
	Order By amo.Fecha_Pago
)
 
OPEN qryPrestamos;  
FETCH NEXT FROM qryPrestamos 
INTO @NombreCliente, @NombrePrestamdor, @MontoAdeudado, @MontoDesembolsado, @FechaDesembolsado, @PrestamoNo;

WHILE @@FETCH_STATUS = 0  
   BEGIN  	

	DECLARE @AmortizacionNo As Integer = 0;	
	DECLARE @prioridad as Integer = 30;
	DECLARE @from AS VARCHAR(500) = '';
	DECLARE @to AS VARCHAR(2000) = '';
	DECLARE @subject AS VARCHAR(500) = '';
	DECLARE @body as Varchar(Max) = '
	<!DOCTYPE html>
	<html>
		<head>
			<title>Recordatorio de Pago</title>
		</head>
		<body style="font-family: Arial, Helvetica, sans-serif;">
			<p>
				Hola {NombreCliente},
			</p>
			<br />
			<p>
				Por favor recordar hacer el pago de RD$ {MontoAdeudado} que corresponde a la suma del total adeudado al día de hoy. Por favor evite mora e intereses por atrasos.
			</p>
			<br />
			<p>
				A continuación detalle del monto:
			</p>
			<table border="1" cellpadding="3" style="font-family: Arial, Helvetica, sans-serif;">
				<tr>
					<td align="left">Monto capital:</td>
					<td align="right">RD$ {MontoDesembolsado}</td>
				</tr>
				<tr>
					<td align="left">Intereses para el {fecha}:</td>
					<td align="right">{interes}</td>
				</tr>
				<tr>
					<td align="right"><b>TOTAL:</b></td>
					<td align="right"><b>{MontoAdeudado}</b></td>
				</tr>
			</table>
			<p>
				Si ya ha realizado el pago por favor desestimar este mensaje.
			</p>
			<br />
			<p>
				Recordar que el Desembolso fue realizado el día {FechaDesembolsado}, por el Sr. {NombrePrestamdor} al Sr. {NombreCliente}.
			</p>
			<br />
			<p>Saludos Cordiales,</p>
			<p>Easy Loan App</p>
		</body>
	</html>'
	
	SET @body = REPLACE(@body, '{NombreCliente}', @NombreCliente);
	SET @body = REPLACE(@body, '{NombrePrestamdor}', @NombrePrestamdor);
	SET @body = REPLACE(@body, '{MontoAdeudado}', @MontoAdeudado);
	SET @body = REPLACE(@body, '{MontoDesembolsado}', @MontoDesembolsado);
	SET @body = REPLACE(@body, '{FechaDesembolsado}', @FechaDesembolsado);
	SET @body = REPLACE(@body, '{PrestamoNo}', @PrestamoNo);
		
	Select Top 1 @AmortizacionNo = Amortizacion_No 
	From Amortizacion 
	Where @PrestamoNo = Prestamo_No and Pagado = 0 and Fecha_Pago < GETDATE() and isnull(Notificado1, 0) = 0
	Order By Fecha_Pago
	
	--Enviar Paso 20
	INSERT INTO [dbTestMails].[dbo].[CorreosEnviar]
			   ([From]
			   ,[To]
			   ,[Subject]
			   ,[Body]
			   ,[UsuarioId]
			   ,[AppId]
			   ,[Enviado]
			   ,[FechaEnviado]
			   ,[Error]
			   ,[DetalleError]
			   ,[FechaError]
			   ,[Reintentos]
			   ,[FechaEnviar]
			   ,[Prioridad]
			   ,[FechaCreacion])
		 VALUES
			   (@from
			   ,@to
			   ,@subject
			   ,@body
			   ,NULL
			   ,30
			   ,0
			   ,NULL
			   ,0
			   ,NULL
			   ,NULL
			   ,NULL
			   ,DATEADD(MINUTE,@prioridad, GETDATE())
			   ,@prioridad
			   ,GETDATE())



	--Alter Table 30
	Update Amortizacion 
	Set Notificado1 = 1 
	Where Amortizacion_No = @AmortizacionNo


  
--WHILE ( SELECT AVG(ListPrice) FROM dbo.DimProduct) < $300  
--BEGIN  
--    UPDATE dbo.DimProduct  
--        SET ListPrice = ListPrice * 2;  
--    SELECT MAX ( ListPrice) FROM dbo.DimProduct  
--    IF ( SELECT MAX (ListPrice) FROM dbo.DimProduct) > $500  
--        BREAK;  
--END  
  

		
		FETCH NEXT FROM qryPrestamos 
		INTO @NombreCliente, @NombrePrestamdor, @MontoAdeudado, @MontoDesembolsado, @FechaDesembolsado, @PrestamoNo;
   END;  
CLOSE qryPrestamos;  
DEALLOCATE qryPrestamos;  
GO  

