﻿using LoadData.DataSourse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadData.LogErrores
{
    public static class Log
    {
		public static void Error(Exception ex, Pelicula pelicula)
		{
			Console.WriteLine("Error:");
			Console.WriteLine("Pelicula: " + pelicula.Nombre);
			Console.WriteLine("UrlLink: " + pelicula.UrlLink);
			Console.WriteLine("Descripcion: " + ex.Message);
			var nuevoDB = new dbPeliculasDataContext();
			var nuevoError = new LogErrore();
			nuevoError.Nombre = pelicula.Nombre;
			nuevoError.LinkUrl = pelicula.UrlLink;
			nuevoError.Error = ex.Message + " | " + ex.InnerException + " | " + ex.StackTrace;
			nuevoError.createdDate = DateTime.Now;
			nuevoDB.LogErrores.InsertOnSubmit(nuevoError);
			nuevoDB.SubmitChanges();
		}
		public static void Error(Exception ex, string nombre)
		{
			var nuevoDB = new dbPeliculasDataContext();
			var nuevoError = new LogErrore();
			nuevoError.Nombre = nombre;
			nuevoError.LinkUrl = nombre;
			nuevoError.Error = ex.Message + " | " + ex.InnerException + " | " + ex.StackTrace;
			nuevoError.createdDate = DateTime.Now;
			nuevoDB.LogErrores.InsertOnSubmit(nuevoError);
			nuevoDB.SubmitChanges();
		}
		public static void Error(string nombre, string linkUrl, string error)
		{
			var nuevoDB = new dbPeliculasDataContext();
			var nuevoError = new LogErrore();
			nuevoError.Nombre = nombre;
			nuevoError.LinkUrl = linkUrl;
			nuevoError.Error = error;
			nuevoError.createdDate = DateTime.Now;
			nuevoDB.LogErrores.InsertOnSubmit(nuevoError);
			nuevoDB.SubmitChanges();
		}
    }
}
