﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoadData.KeyCapture
{
	public class LetrasQueNoDebenSalir
	{
		private static string jsonLista
		{
			get
			{
				return ConfigurationManager.AppSettings.Get("jsonLista");
			}
		}
		private static List<objCaracter> lista
		{
			get
			{
				return Newtonsoft.Json.JsonConvert.DeserializeObject<List<objCaracter>>(jsonLista); ;
			}
		}
		public static string GetCaracter(int vkCode)
		{

			if (vkCode == 13)
				return Environment.NewLine;

			var obj = lista.Where(p => p.ValorInt == vkCode).FirstOrDefault();
			if (obj != null)
			{
				if (obj.Imprimir)
				{
					if (!string.IsNullOrEmpty(obj.Representa))
						return obj.Representa;
					return " {" + ((System.Windows.Forms.Keys)vkCode).ToString() + "-" + vkCode.ToString() + "} ";
				}
				else
				{
					return "";
				}
			}

			return ((System.Windows.Forms.Keys)vkCode).ToString();
		}
		private class objCaracter
		{
			public string Nombre { get; set; }
			public string Description { get; set; }
			public int ValorInt { get; set; }
			public bool Imprimir { get; set; }
			public string Representa { get; set; }
		}

	}
	
}
