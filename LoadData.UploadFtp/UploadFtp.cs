﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilities.FTP;
using System.Configuration;
using System.IO;

namespace LoadData.UploadFtp
{
	public static class UploadFtp
	{
		public static bool UploadFile(string rutaArchivo, string nombreArchivo)
		{
            var config = new ConfigData(true);
			FTPclient ftp = new FTPclient(config.ServidorFTP + ":" + config.PuertoFTP, config.LoginAFP, config.Password);
			return ftp.Upload(rutaArchivo + nombreArchivo, config.RutaCarpetaRemota + nombreArchivo);
		}

        public static bool UploadFile(string filePath, ConfigData config)
        {
            FTPclient ftp = new FTPclient(config.ServidorFTP + ":" + config.PuertoFTP, config.LoginAFP, config.Password);
            return ftp.Upload(filePath, config.RutaCarpetaRemota + Path.GetFileName(filePath));
        }
    }
}
