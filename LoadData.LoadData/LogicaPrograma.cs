﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadData.DataSourse;
using System.Data.Linq;
using WordPressSharp;
using WordPressSharp.Models;
using System.Diagnostics;
using System.Configuration;

namespace LoadData.LoadData
{
	public class LogicaPrograma
	{
		private ICargarDatos currentClass;
		public LogicaPrograma() 
		{
			currentClass = new CargarDatosYify();
		}
		public void CargarPeliculasAlWordPressOnline(ref EventLog eventLog)
		{
			var _db = new dbPeliculasDataContext();
			var nuevaPelicula = _db.Peliculas.Where(p => p.Posted == false && p.Host == currentClass.Hospedaje.Link).OrderByDescending(orden => orden.PeliculaId);
			foreach (Pelicula item in nuevaPelicula)
			{
				if (item != null)
				{
					string enlaces = getValoresDescargar();
					item.PostedId = CargarWordPress.CargarPeliculaWordPress(item, enlaces, -1);
					item.Posted = true;
					_db.SubmitChanges();
					if (eventLog == null)
					{
						Console.WriteLine("Una Pelicula mas....");
					}
					else
					{
						eventLog.WriteEntry("Una Pelicula mas....", EventLogEntryType.Information, 2);
					}
				}
			}
		}
		public void HiloCentralRobarLasPeliculasConsole(bool isCargarPeliculas, ref EventLog eventLog)
		{
			if (isCargarPeliculas)
			{
				int startPaginas = int.Parse(ConfigurationManager.AppSettings.Get("startPaginas"));
				int totalPaginas = int.Parse(ConfigurationManager.AppSettings.Get("totalPaginas"));
				string categoriaImportar = currentClass.Hospedaje.CategoriaImportar; // "/categoria/1/estrenos"; // "/browse-movies"
				
				for (int i = startPaginas; i <= totalPaginas; ++i)
				{
					if (eventLog == null)
					{
						Console.WriteLine("Comienza: " + i.ToString());
					}
					else
					{
						eventLog.WriteEntry("Comienza: Tomar Peliculas Header" + i.ToString(), EventLogEntryType.Information, 2);
					}
					string urlOpcional2 = "";
					if (i > 1) {
						urlOpcional2 = currentClass.Hospedaje.ContadorPagina + i.ToString();
					}

					GuardarCabezera(categoriaImportar + urlOpcional2);
					if (eventLog == null)
					{
						Console.WriteLine("Termina..." + i.ToString());
						Console.WriteLine(".");
					}
					else
					{
						eventLog.WriteEntry("Termina:  " + urlOpcional2 + " - " + i.ToString(), EventLogEntryType.Information, 2);
					}
				}
			}
			else
			{
				if (eventLog == null)
				{
					Console.WriteLine("Comienza: ");
				}
				else
				{
					eventLog.WriteEntry("Comienza: Tomar Peliculas Detalles", EventLogEntryType.Information, 2);
				}
				GuardarDetalle();
				if (eventLog == null)
				{
					Console.WriteLine("Termina...");
					Console.WriteLine(".");
				}
				else
				{
					eventLog.WriteEntry("Termina... Tomar Peliculas Detalles", EventLogEntryType.Information, 2);
				}
			}
		}
		public List<Pelicula> GuardarCabezera(string urlOpcional2)
		{
			var listaPeliculas = currentClass.GetPeliculasHeader(urlOpcional2);
			currentClass.GuardarPeliculasHeader(listaPeliculas);
			return listaPeliculas;
		}
		public IQueryable<Pelicula> GuardarDetalle()
		{
			var _db = new dbPeliculasDataContext();
			var listaPeliculas = _db.Peliculas.Where(p => p.DetalleLoaded == false).OrderBy(peli => peli.PeliculaId);
			currentClass.GetPeliculasDetails(listaPeliculas);
			currentClass.GuardarPeliculasDetails(listaPeliculas, false, _db);
			return listaPeliculas;
		}
		public string getValoresDescargar()
		{
			string retVal = "";

			if (currentClass.Hospedaje.HospedajeId == 2)
			{
				retVal += "<div class='clear'></div>";
				retVal += "<div class='anuncio_contenido'></div>";
				retVal += "<div class='clear'></div>";
				retVal += "<nav class='navigation post-navigation' role='navigation'>";
				retVal += "		<h1 class='screen-reader-text'>Archivos de Descargar</h1>";
				retVal += "		<div class='nav-link'>";
				retVal += "		<a href='{0}' rel='prev'>";
				retVal += "			<span class='nav-next'>Descargar Magnet Link</span>";
				retVal += "		</a>";
				retVal += "		<a href='http://torrentdatabase.org/{1}' rel='next'>";
				retVal += "			<span class='nav-next'>Descargar Torrent</span>";
				retVal += "		</a>";
				retVal += "		</div>";
				retVal += "</nav>";
			}
			else {
				retVal += "<div class='clear'></div>";
				retVal += "<div class='anuncio_contenido'></div>";
				retVal += "<div class='clear'></div>";
				retVal += "<nav class='navigation post-navigation' role='navigation'>";
				retVal += "		<h1 class='screen-reader-text'>Archivos de Descargar</h1>";
				retVal += "		<div class='nav-link'>";
				retVal += "		<a href='{0}' rel='prev'>";
				retVal += "			<span class='nav-next'>Descargar Magnet Link</span>";
				retVal += "		</a>";
				retVal += "		<a href='http://torrentdatabase.org/TorrentsFiles/{1}' rel='next'>";
				retVal += "			<span class='nav-next'>Descargar Torrent</span>";
				retVal += "		</a>";
				retVal += "		</div>";
				retVal += "</nav>";
			}
			return retVal;
		}
	}
}
