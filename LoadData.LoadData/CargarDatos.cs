﻿using CsQuery;
using LoadData.DataSourse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Data;
using System.Data.Linq;
using LoadData.LogErrores;

namespace LoadData.LoadData
{
	public abstract class CargarDatos : ICargarDatos
	{
		//public abstract string ContadorPagina { get; }
		public abstract Hospedaje Hospedaje { get; }
		public abstract List<Pelicula> GetPeliculasHeader(string urlOpcional = "");
		public abstract void GuardarPeliculasHeader(List<Pelicula> nuevaLista, bool isInsert = true, dbPeliculasDataContext db = null);
		public abstract void GuardarPeliculasDetails(IQueryable<Pelicula> nuevaLista, bool isInsert = true, dbPeliculasDataContext db = null);
		public abstract void GetPeliculasDetails(IQueryable<Pelicula> retVal);
		public string GetHtml(string urlLink) {
			string retVal = "";
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlLink);
			HttpWebResponse respose = (HttpWebResponse)request.GetResponse();
			StreamReader sr = new StreamReader(respose.GetResponseStream());
			retVal = sr.ReadToEnd();
			return retVal;
		}
		public void AgregarPropiedad(Pelicula pelicula, int propiedadId, string valorPropiedad) 
		{
			var peliculaPropiedad = new PeliculasPropiedad()
			{
				PropiedadId = propiedadId, // CodigoPropiedad.LinkUrlImagen,
				Valor = valorPropiedad //urlLink + "/" + imagen.GetAttribute("src")
			};
			pelicula.PeliculasPropiedads.Add(peliculaPropiedad);
		}

	}
}
