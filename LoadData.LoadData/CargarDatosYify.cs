﻿using CsQuery;
using LoadData.DataSourse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Data;
using System.Data.Linq;
using LoadData.LogErrores;

namespace LoadData.LoadData
{
	public class CargarDatosYify : CargarDatos
	{
		private Hospedaje _hospedaje;
		public override Hospedaje Hospedaje
		{
			get
			{
				if (_hospedaje == null)
				{
					int _hospedajeId = 2;
					var db = new dbPeliculasDataContext();
					_hospedaje = db.Hospedajes.Where(p => p.HospedajeId == _hospedajeId).FirstOrDefault();
				}
				return _hospedaje;
			}
		}
		public override List<Pelicula> GetPeliculasHeader(string urlOpcional = "")
		{
			var retVal = new List<Pelicula>();
			string _urlLink = Hospedaje.Link;
			if (urlOpcional != "")
			{
				_urlLink += urlOpcional;
			}
			CQ objCQ = GetHtml(_urlLink);
			var rows = objCQ[Hospedaje.ClaseBuscarHeader];

			int cantidadPeliculas = 4;
			int indice = 0;
			foreach (var row in rows)
			{
				indice += 1;
				var nuevaPelicula = new Pelicula();
				nuevaPelicula.FechaCreacion = DateTime.Now;
				nuevaPelicula.Host = Hospedaje.Link;
				procesarPropiedadesDetalles(nuevaPelicula, row.Cq(), ClasePagina.Header);
				retVal.Add(nuevaPelicula);

				if (indice > cantidadPeliculas)
				{
					break;
				}
			}

			return retVal;
		}
		public override void GuardarPeliculasHeader(List<Pelicula> nuevaLista,
			bool isInsert = true, dbPeliculasDataContext db = null)
		{

			if (db == null)
			{
				db = new dbPeliculasDataContext();
			}

			DescargarArchivoDetalle(nuevaLista.AsQueryable(), isInsert, db, false);
			DescargarArchivoDetalle(nuevaLista.AsQueryable(), isInsert, db, true);

		}
		public override void GuardarPeliculasDetails(IQueryable<Pelicula> nuevaLista, bool isInsert = true, dbPeliculasDataContext db = null)
		{
			if (db == null)
			{
				db = new dbPeliculasDataContext();
			}
			DescargarArchivoDetalle(nuevaLista, isInsert, db, false);
			DescargarArchivoDetalle(nuevaLista, isInsert, db, true);
		}
		private void DescargarArchivoDetalle(IQueryable<Pelicula> nuevaLista, bool isInsert, dbPeliculasDataContext db, bool descargaDeArchivos = false)
		{

			foreach (Pelicula pelicula in nuevaLista)
			{
				if (descargaDeArchivos)
				{
					descargaArchivoImagen(db, pelicula);
					descargarArchivoTorrent(db, pelicula);
				}
				else if (isInsert)
				{
					db.Peliculas.InsertOnSubmit(pelicula);
				}
				else
				{
					pelicula.DetalleLoaded = true;
				}

				try
				{
					db.SubmitChanges();
				}
				catch (Exception ex)
				{
					Console.WriteLine("Error:");
					Console.WriteLine("Pelicula: " + pelicula.Nombre);
					Console.WriteLine("UrlLink: " + pelicula.UrlLink);
					Console.WriteLine("Descripcion: " + ex.Message);
					var nuevoDB = new dbPeliculasDataContext();
					var nuevoError = new LogErrore();
					nuevoError.Nombre = pelicula.Nombre;
					nuevoError.LinkUrl = pelicula.UrlLink;
					nuevoError.Error = ex.Message + " | " + ex.InnerException + " | " + ex.StackTrace;
					nuevoError.createdDate = DateTime.Now;
					nuevoDB.LogErrores.InsertOnSubmit(nuevoError);
					nuevoDB.SubmitChanges();
				}
			}
		}
		private void descargarArchivoTorrent(dbPeliculasDataContext db, Pelicula pelicula)
		{
			if (!Directory.Exists(Hospedaje.DirectorioTorrents))
			{
				Directory.CreateDirectory(Hospedaje.DirectorioTorrents);
			}
			
			foreach (PeliculasPropiedad propiedad in pelicula.PeliculasPropiedads)
			{
				propiedad.Propiedade = db.Propiedades.Where(p => p.PropiedadId == propiedad.PropiedadId).FirstOrDefault();

				if (propiedad.Propiedade.PropiedadTipoId == TipoPropiedad.Archivos)
				{
					if (propiedad.Valor != null && propiedad.Descargado == false)
					{
						char[] separators = { Convert.ToChar("/") };
						var nombreTorrent = propiedad.Valor.Split(separators).Last();
						if (nombreTorrent != null)
						{
							using (WebClient webClient = new WebClient())
							{
								nombreTorrent = pelicula.PeliculaId.ToString() + "_" + propiedad.PropiedadId.ToString() + "_" + nombreTorrent;
								if (!nombreTorrent.Contains("."))
								{
									string extencion = (String.IsNullOrEmpty(propiedad.Propiedade.Extencion) ? "" : propiedad.Propiedade.Extencion);
									nombreTorrent = nombreTorrent + extencion;
								}
								if (!File.Exists(Hospedaje.DirectorioTorrents + nombreTorrent))
								{
									webClient.DownloadFile(propiedad.Valor, Hospedaje.DirectorioTorrents + nombreTorrent);
									propiedad.ValorLocal = Hospedaje.DirectorioTorrents + nombreTorrent;
									propiedad.Descargado = true;
								}
							}
						}
					}
				}
			}
		}
		private void descargaArchivoImagen(dbPeliculasDataContext db, Pelicula pelicula)
		{
			if (!Directory.Exists(Hospedaje.DirectorioImagenes))
			{
				Directory.CreateDirectory(Hospedaje.DirectorioImagenes);
			}
			foreach (PeliculasPropiedad propiedad in pelicula.PeliculasPropiedads)
			{
				propiedad.Propiedade = db.Propiedades.Where(p => p.PropiedadId == propiedad.PropiedadId).FirstOrDefault();

				if (propiedad.Propiedade.PropiedadTipoId == TipoPropiedad.LinkUrlImagen)
				{
					if (propiedad.Valor != null && propiedad.Descargado == false)
					{
						char[] separators = { Convert.ToChar("/") };
						var nombreImagen = propiedad.Valor.Split(separators).Last();
						if (nombreImagen != null)
						{
							using (WebClient webClient = new WebClient())
							{
								nombreImagen = pelicula.PeliculaId.ToString() + "_" + propiedad.PropiedadId.ToString() + "_" + nombreImagen;
								if (!nombreImagen.Contains("."))
								{
									string extencion = (String.IsNullOrEmpty(propiedad.Propiedade.Extencion) ? "" : propiedad.Propiedade.Extencion);
									nombreImagen = nombreImagen + extencion;
								}
									
								if (!File.Exists(Hospedaje.DirectorioTorrents + nombreImagen))
								{
									webClient.DownloadFile(propiedad.Valor, Hospedaje.DirectorioImagenes + nombreImagen);
									propiedad.ValorLocal = Hospedaje.DirectorioImagenes + nombreImagen;
									propiedad.Descargado = true;
								}
							}
						}
					}
				}
			}
		}
		public override void GetPeliculasDetails(IQueryable<Pelicula> retVal)
		{
			foreach (Pelicula item in retVal)
			{
				//var textoHTML = GetHtml(item.UrlLink);
				CQ objCQ = GetHtml(item.UrlLink);
				var rows1 = objCQ[Hospedaje.ClaseBuscarDetails];
				procesarPropiedadesDetalles(item, rows1, ClasePagina.Details);
				if (Hospedaje.ClaseBuscarOpcional != null)
				{
					var rows2 = objCQ[Hospedaje.ClaseBuscarOpcional];
					procesarPropiedadesDetalles(item, rows2, ClasePagina.Opcional);
				}
			}
		}
		private void procesarPropiedadesDetalles(Pelicula pelicula, CQ row, int ClaseBuscarId)
		{
			foreach (Propiedade item in Hospedaje.Propiedades.Where(cla => cla.ClaseBuscarId == ClaseBuscarId))
			{
				if (!String.IsNullOrEmpty(item.ClaseBuscar))
				{
					var tempObj = row.Find(item.ClaseBuscar).FirstOrDefault();
					if (item.ClaseBuscar == "=" && item.Indice != null)
					{
						tempObj = row[(int)item.Indice];
					}

					if (tempObj != null)
					{
						switch (item.PropiedadTipoId)
						{
							case TipoPropiedad.InnerText:
								AgregarPropiedad(pelicula, item.PropiedadId, tempObj.InnerText);
								break;
							case TipoPropiedad.LinkUrlImagen:
								if (tempObj.GetAttribute("data-cfsrc") != null)
								{
									AgregarPropiedad(pelicula, item.PropiedadId, getUrlImagenOArchivo(item.Hospedaje.Link, tempObj.GetAttribute("data-cfsrc")));
								}
								else
								{
									AgregarPropiedad(pelicula, item.PropiedadId, getUrlImagenOArchivo(item.Hospedaje.Link, tempObj.GetAttribute("src")));
								}
								break;
							case TipoPropiedad.RenderSelection:
								string selectionHtml = row[item.ClaseBuscar].RenderSelection();
								AgregarPropiedad(pelicula, item.PropiedadId, selectionHtml);
								break;
							case TipoPropiedad.UrlLink:
								pelicula.UrlLink = item.Hospedaje.Link + tempObj.GetAttribute("href");
								AgregarPropiedad(pelicula, item.PropiedadId, getUrlImagenOArchivo(item.Hospedaje.Link, tempObj.GetAttribute("href")));
								break;
							case TipoPropiedad.Nombre:
								pelicula.Nombre = tempObj.InnerText;
								AgregarPropiedad(pelicula, item.PropiedadId, tempObj.InnerText);
								break;
							case TipoPropiedad.MagnetLink:
								AgregarPropiedad(pelicula, item.PropiedadId, tempObj.GetAttribute("href"));
								break;
							case TipoPropiedad.Archivos:
								//string extencion = (String.IsNullOrEmpty(item.Extencion) ? "" : item.Extencion);
								AgregarPropiedad(pelicula, item.PropiedadId, getUrlImagenOArchivo(item.Hospedaje.Link, tempObj.GetAttribute("href")));
								break;
						}
					}
				}
			}

			foreach (var itemColeccion in Hospedaje.Propiedades.Where(p => p.PropiedadTipoId == TipoPropiedad.Coleccion && p.ClaseBuscarId == ClaseBuscarId))
			{
				if (itemColeccion != null)
				{
					int contador = 0;
					var tempColeccion = row.Find(itemColeccion.ClaseBuscar);
					//if (itemColeccion.ClaseBuscar == "=")
					//{
					//	tempColeccion = row;
					//}
					var propiedadesColeccion = Hospedaje.Propiedades.Where(p => p.ColeccionPadreId == itemColeccion.PropiedadId).ToList();

					foreach (var informacion in tempColeccion)
					{
						var propiedadInsertar = propiedadesColeccion.Where(p => p.Indice == contador).FirstOrDefault();
						if (propiedadInsertar != null)
						{
							switch (propiedadInsertar.PropiedadTipoId)
							{
								case TipoPropiedad.InnerText:
									AgregarPropiedad(pelicula, propiedadInsertar.PropiedadId, informacion.InnerText);
									break;
								case TipoPropiedad.LinkUrlImagen:
									if (informacion.GetAttribute("data-cfsrc") != null)
									{
										AgregarPropiedad(pelicula, propiedadInsertar.PropiedadId, getUrlImagenOArchivo(propiedadInsertar.Hospedaje.Link, informacion.GetAttribute("data-cfsrc")));
									}
									else
									{
										AgregarPropiedad(pelicula, propiedadInsertar.PropiedadId, getUrlImagenOArchivo(propiedadInsertar.Hospedaje.Link, informacion.GetAttribute("src")));
									}
									break;
								case TipoPropiedad.MagnetLink:
									AgregarPropiedad(pelicula, propiedadInsertar.PropiedadId, informacion.GetAttribute("href"));
									break;
								case TipoPropiedad.Archivos:
									//string extencion = (String.IsNullOrEmpty(propiedadInsertar.Extencion) ? "" : propiedadInsertar.Extencion);
									AgregarPropiedad(pelicula, propiedadInsertar.PropiedadId, getUrlImagenOArchivo(propiedadInsertar.Hospedaje.Link, informacion.GetAttribute("href")));
									break;

							}
						}
						contador += 1;
					}
				}
			}
		}
		private string getUrlImagenOArchivo(string hostUrl, string url)
		{
			if (!String.IsNullOrEmpty(url))
			{
				if (url.Contains("http://") || url.Contains("https://"))
				{
					return url;
				}
				else
				{
					return hostUrl + "/" + url;
				}
			}
			return hostUrl;
		}
	}
}
