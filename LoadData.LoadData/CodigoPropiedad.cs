﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadData.LoadData
{
	struct CodigoPropiedad
	{
		public const int NombreImagen = 1;
		public const int LinkUrlImagen = 2;
		public const int Categoria = 3;
		public const int Antiguedad = 4;
		public const int ValoracionMedia = 5;
		public const int ValoracionAV = 6;
		public const int NombreImagenGrande = 7;
		public const int LinkUrlImagenGrande = 8;
		public const int Fecha = 9;
		public const int Popularidad = 10;
		public const int Tamano = 11;
		public const int TituloOriginal = 12;
		public const int Ano = 13;
		public const int Duracion = 14;
		public const int Pais = 15;
		public const int Director = 16;
		public const int Guion = 17;
		public const int Fotografia = 18;
		public const int Reparto = 19;
		public const int Productora = 20;
		public const int Genero = 21;
		public const int Musica = 22;
		public const int Sinopsis = 23;
		public const int Detalles = 24;
		public const int ArchivoTorrent = 25;
		public const int ArchivoMagneLink = 26;
		public const int ArchivoTorrentName = 27;

	}

	public class CodigoPropiedadPorClase
	{
		private int _hospedajeId;
		public CodigoPropiedadPorClase(int hospedajeId) 
		{
			_hospedajeId = hospedajeId;
		}
		public int ArchivoMagneLink 
		{
			get 
			{
				if (_hospedajeId == 1) {
					return 26; 
				}
				if (_hospedajeId == 2)
				{
					return 0; 
				}
				return 26; 
			}
		}
		public int NombreImagen
		{
			get
			{
				if (_hospedajeId == 1)
				{
					return 1;
				}
				if (_hospedajeId == 2)
				{
					return 33;
				}
				return 1;
			}
		}
		public int ArchivoTorrentName
		{
			get
			{
				if (_hospedajeId == 1)
				{
					return 27;
				}
				if (_hospedajeId == 2)
				{
					return 36;
				}
				return 27;
			}
		}
		public int Detalles
		{
			get
			{
				if (_hospedajeId == 1)
				{
					return 24;
				}
				if (_hospedajeId == 2)
				{
					return 39;
				}
				return 24;
			}
		}
	}
}
