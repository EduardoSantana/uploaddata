﻿using CsQuery;
using LoadData.DataSourse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Data;
using System.Data.Linq;
using LoadData.LogErrores;

namespace LoadData.LoadData
{
	public class CargarDatosElitetorrent : CargarDatos
	{
		private const string _claseBuscar1 = "#box-ficha";
		private const string _claseBuscar2 = "a.enlace_torrent";
		private const string _Host = @"http://www.elitetorrent.net";
		private const string _ClaseBuscarHeader = "ul.miniboxs li";
		private const string _DirectorioImagenes = @"ImagenThumbs\";
		private const string _DirectorioTorrentsFiles = @"TorrentsFiles\";
		private Hospedaje _hospedaje;
		public override Hospedaje Hospedaje
		{
			get
			{
				int _hospedajeId = 1;
				var db = new dbPeliculasDataContext();
				if (_hospedaje == null)
				{
					_hospedaje = db.Hospedajes.Where(p => p.HospedajeId == _hospedajeId).FirstOrDefault();
				}
				return _hospedaje;
			}
		}
		private string ContadorPagina { get { return "/pag:"; } }
		public override List<Pelicula> GetPeliculasHeader(string urlOpcional = "")
		{
			var retVal = new List<Pelicula>();
			string _urlLink = _Host;
			if (urlOpcional != "")
			{
				_urlLink += urlOpcional;
			}
			CQ objCQ = GetHtml(_urlLink);
			var rows = objCQ[_ClaseBuscarHeader];

			foreach (var row in rows)
			{
				var nuevaPelicula = new Pelicula();
				nuevaPelicula.FechaCreacion = DateTime.Now;
				nuevaPelicula.Host = _Host;
				var imagen = row.Cq().Find("a img").FirstOrDefault();
				if (imagen != null)
				{
					AgregarPropiedad(nuevaPelicula,
						CodigoPropiedad.LinkUrlImagen,
						_Host + "/" + imagen.GetAttribute("src"));

					char[] separators = { Convert.ToChar("/") };
					AgregarPropiedad(nuevaPelicula,
						CodigoPropiedad.NombreImagen,
						imagen.GetAttribute("src").Split(separators).Last());

				}

				var valoracionMedia = row.Cq().Find("div .voto1").FirstOrDefault();
				if (valoracionMedia != null)
				{
					AgregarPropiedad(nuevaPelicula,
						CodigoPropiedad.ValoracionMedia,
						valoracionMedia.InnerText);
				}

				var valoracionAudioVideo = row.Cq().Find("div .voto2").FirstOrDefault();
				if (valoracionAudioVideo != null)
				{
					AgregarPropiedad(nuevaPelicula,
						CodigoPropiedad.ValoracionAV,
						valoracionAudioVideo.InnerText);
				}

				var nombre = row.Cq().Find("div .nombre").FirstOrDefault();
				if (nombre != null)
				{
					nuevaPelicula.Nombre = nombre.InnerText;
					nuevaPelicula.UrlLink = _Host + nombre.GetAttribute("href");
				}

				var categoria = row.Cq().Find("div .categoria").FirstOrDefault();
				if (categoria != null)
				{
					AgregarPropiedad(nuevaPelicula,
						CodigoPropiedad.Categoria,
						categoria.InnerText);
				}

				var fecha = row.Cq().Find("div .fecha").FirstOrDefault();
				if (fecha != null)
				{
					AgregarPropiedad(nuevaPelicula,
						CodigoPropiedad.Antiguedad,
						fecha.InnerText);
				}

				retVal.Add(nuevaPelicula);

			}

			return retVal;
		}
		public override void GuardarPeliculasHeader(List<Pelicula> nuevaLista, 
			bool isInsert = true, dbPeliculasDataContext db = null)
		{
			string directorio = _DirectorioImagenes;
			if (db == null)
			{
				db = new dbPeliculasDataContext();
			}

			foreach (Pelicula pelicula in nuevaLista)
			{
				if (directorio != "")
				{
					var nombreImagen = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == CodigoPropiedad.NombreImagen).FirstOrDefault();
					if (nombreImagen != null)
					{
						using (WebClient webClient = new WebClient())
						{
							if (!File.Exists(directorio + nombreImagen.Valor))
							{
								var LinkUrlImagen = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == CodigoPropiedad.LinkUrlImagen).FirstOrDefault();
								webClient.DownloadFile(LinkUrlImagen.Valor, directorio + nombreImagen.Valor);
							}
						}
					}
				}

				if (isInsert)
				{
					db.Peliculas.InsertOnSubmit(pelicula);
				}
				try
				{
					db.SubmitChanges();
				}
				catch (Exception ex)
				{
					Log.Error(ex, pelicula);
				}
			}
		}
		public override void GuardarPeliculasDetails(IQueryable<Pelicula> nuevaLista, bool isInsert = true, dbPeliculasDataContext db = null)
		{
			string directorio = _DirectorioImagenes;
			string directorioTorrent = _DirectorioTorrentsFiles;
			if (db == null)
			{
				db = new dbPeliculasDataContext();
			}

			foreach (Pelicula pelicula in nuevaLista)
			{
				if (directorio != "")
				{
					var nombreImagen = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == CodigoPropiedad.NombreImagenGrande).FirstOrDefault();
					if (nombreImagen != null)
					{
						using (WebClient webClient = new WebClient())
						{
							if (!File.Exists(directorio + nombreImagen.Valor))
							{
								var LinkUrlImagen = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == CodigoPropiedad.LinkUrlImagenGrande).FirstOrDefault();
								webClient.DownloadFile(LinkUrlImagen.Valor, directorio + nombreImagen.Valor);
							}
						}
					}
				}
				if (directorioTorrent != "")
				{
					var torrentName = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == CodigoPropiedad.ArchivoTorrentName).FirstOrDefault();
					if (torrentName != null)
					{
						using (WebClient webClient = new WebClient())
						{
							if (!File.Exists(directorioTorrent + torrentName.Valor))
							{
								var linkUrlTorrrent = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == CodigoPropiedad.ArchivoTorrent).FirstOrDefault();
								webClient.DownloadFile(linkUrlTorrrent.Valor, directorioTorrent + torrentName.Valor);
							}
						}
					}
				}
				pelicula.DetalleLoaded = true;
				if (isInsert)
				{
					db.Peliculas.InsertOnSubmit(pelicula);
				}

				try
				{
					db.SubmitChanges();
				}
				catch (Exception ex)
				{
					Console.WriteLine("Error:");
					Console.WriteLine("Pelicula: " + pelicula.Nombre);
					Console.WriteLine("UrlLink: " + pelicula.UrlLink);
					Console.WriteLine("Descripcion: " + ex.Message);
					var nuevoDB = new dbPeliculasDataContext();
					var nuevoError = new LogErrore();
					nuevoError.Nombre = pelicula.Nombre;
					nuevoError.LinkUrl = pelicula.UrlLink;
					nuevoError.Error = ex.Message + " | " + ex.InnerException + " | " + ex.StackTrace;
					nuevoError.createdDate = DateTime.Now;
					nuevoDB.LogErrores.InsertOnSubmit(nuevoError);
					nuevoDB.SubmitChanges();
				}
			}
		}
		public override void GetPeliculasDetails(IQueryable<Pelicula> retVal)
		{
			foreach (Pelicula item in retVal)
			{
				var textoHTML = GetHtml(item.UrlLink);
				CQ objCQ = GetHtml(item.UrlLink);
				var rows1 = objCQ[_claseBuscar1];
				cargarDetalles(item, rows1);
				var rows2 = objCQ[_claseBuscar2];
				cargarDetallesLinksTorrent(item, rows2);
			}
		}
		private void cargarDetalles(Pelicula item, CQ rows1)
		{
			var imagen = rows1.Find("div.secc-izq img").FirstOrDefault();
			if (imagen != null)
			{
				AgregarPropiedad(item,
					CodigoPropiedad.LinkUrlImagenGrande,
					item.Host + "/" + imagen.GetAttribute("src"));

				char[] separators = { Convert.ToChar("/") };
				AgregarPropiedad(item,
					CodigoPropiedad.NombreImagenGrande,
					imagen.GetAttribute("src").Split(separators).Last());
			}
			var informacionTecnica = rows1.Find("dl.info-tecnica dd");
			int _contadorInfomacion = 1;
			foreach (var informacion in informacionTecnica)
			{
				if (_contadorInfomacion == 1)
				{
					AgregarPropiedad(item,
						CodigoPropiedad.Fecha,
						informacion.InnerText);
				}
				if (_contadorInfomacion == 3)
				{
					AgregarPropiedad(item,
						CodigoPropiedad.Popularidad,
						informacion.InnerText);
				}
				if (_contadorInfomacion == 4)
				{
					AgregarPropiedad(item,
						CodigoPropiedad.Tamano,
						informacion.InnerText);
				}
				_contadorInfomacion += 1;
			}

			var detalles = rows1.Find(".descrip").FirstOrDefault();
			if (detalles != null)
			{
				string selectionHtml = rows1[".descrip"].RenderSelection();
				AgregarPropiedad(item,
						CodigoPropiedad.Detalles,
						selectionHtml);
			}

		}
		private void cargarDetallesLinksTorrent(Pelicula item, CQ rows2)
		{
			int _contadorInfomacion = 1;
			string variable1 = rows2.RenderSelection();
			foreach (var informacion in rows2)
			{
				if (_contadorInfomacion == 1)
				{
					string _temp = item.Host + informacion.GetAttribute("href");
					AgregarPropiedad(item,
						CodigoPropiedad.ArchivoTorrent,
							_temp);

					char[] separators = { Convert.ToChar("/") };
					AgregarPropiedad(item,
						CodigoPropiedad.ArchivoTorrentName,
						_temp.Split(separators).Last() + ".torrent");

				}
				if (_contadorInfomacion == 2)
				{
					AgregarPropiedad(item,
						CodigoPropiedad.ArchivoMagneLink,
						informacion.GetAttribute("href"));
				}
				_contadorInfomacion += 1;
			}

		}

	}
}
