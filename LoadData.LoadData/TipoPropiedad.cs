﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadData.LoadData
{
	struct TipoPropiedad
	{
		public const int InnerText = 1;
		public const int InnerTextDetalle = 2;
		public const int LinkUrlImagen = 3;
		public const int LinkUrlImagenDetalle = 4;
		public const int UrlLink = 5;
		public const int Nombre = 6;
		public const int RenderSelection = 7;
		public const int RenderSeleccionDetalle = 8;
		public const int Coleccion = 9;
		public const int ColeccionDetalle = 10;
		public const int Archivos = 11;
		public const int ArchivosDetalle = 12;
		public const int MagnetLink = 13;
	}
	struct ClasePagina
	{
		public const int Header = 1;
		public const int Details = 2;
		public const int Opcional = 3;
	}
	
}
