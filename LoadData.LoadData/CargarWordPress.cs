﻿using LoadData.DataSourse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WordPressSharp;
using WordPressSharp.Models;
using LoadData.UploadFtp;
using LoadData.LogErrores;

namespace LoadData.LoadData
{
	public static class CargarWordPress
	{
		private static CodigoPropiedadPorClase codigoPorClase = new CodigoPropiedadPorClase(2);

		public static int CargarPeliculaWordPress(Pelicula pelicula, string enlacesDescarga, double horaSumar)
		{
			int id = 0;
			try
			{
				enlacesDescarga = enlacesDescarga.Replace("{0}", pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == codigoPorClase.ArchivoMagneLink).FirstOrDefault().Valor);
				enlacesDescarga = enlacesDescarga.Replace("{1}", pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == codigoPorClase.ArchivoTorrentName).FirstOrDefault().Valor);

				var post = new Post
				{
					PostType = "post", // "post" or "page"
					Title = pelicula.Nombre,
					Content = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == codigoPorClase.Detalles).FirstOrDefault().Valor + enlacesDescarga,
					PublishDateTime = DateTime.Now.AddHours(horaSumar).ToUniversalTime(),
					Status = "publish" // "draft" or "publish"
				};

				var client = new WordPressClient();
				//You can add a feature image by using the `Data.CreateFromUrl` or `Data.CreateFromFilePath`:
				string nombreImagen = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == codigoPorClase.NombreImagen).FirstOrDefault().Valor;
				var featureImage = Data.CreateFromFilePath(@"ImagenThumbs\" + nombreImagen, "image/jpeg");
				post.FeaturedImageId = client.UploadFile(featureImage).Id;
				id = Convert.ToInt32(client.NewPost(post));

				string nombreTorrent = pelicula.PeliculasPropiedads.Where(p => p.PropiedadId == codigoPorClase.ArchivoTorrentName).FirstOrDefault().Valor;
				UploadFtp.UploadFtp.UploadFile(@"TorrentsFiles\", nombreTorrent);

				bool logTest = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["logTest"]);
				if (logTest)
				{
					string urlLinkPost = client.GetPost(id).Link;
					Log.Error(urlLinkPost, urlLinkPost, urlLinkPost);
				}

			}
			catch (Exception ex)
			{
				Log.Error(ex, pelicula);
			}

			return id;
		}
	}
}
