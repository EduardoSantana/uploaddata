﻿using LoadData.DataSourse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadData.LoadData
{
	public interface ICargarDatos
	{
		Hospedaje Hospedaje { get; }
		List<Pelicula> GetPeliculasHeader(string urlOpcional = "");
		void GuardarPeliculasHeader(List<Pelicula> nuevaLista, bool isInsert = true, dbPeliculasDataContext db = null);
		void GuardarPeliculasDetails(IQueryable<Pelicula> nuevaLista, bool isInsert = true, dbPeliculasDataContext db = null);
		void GetPeliculasDetails(IQueryable<Pelicula> retVal);

	}
}
