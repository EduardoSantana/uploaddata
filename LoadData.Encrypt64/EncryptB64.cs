﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadData.Encrypt64
{
	public class Encrypt
	{
		private static string  SALT1 = "2{m>qF,TSRYcpyZY";
		private static string  SALT2 = "xwRC7~LPv@?Z,G#N";
		private static string  SALT3 = "DYw6vvx7{2a+qDA3";
		private static string  SALT4 = "NX98Ac77McLWzRRC";
		private static string  SALT5 = "xdyH44TNc5wGEbLP";
		private static string  SALT6 = "wMSq7bBb3HhabAej";

		public static string EncryptB64(string s)
		{
            return base64_encode(s); // make work without this
			s = SALT1 + s + SALT2 + SALT3;
			s = base64_encode(s);
			s.Insert(25, SALT4);
			s.Insert(13, SALT5);
			s = SALT1 + SALT2 + s + SALT3;
			s = base64_encode(s);
			s = SALT2 + s + SALT3 + SALT1;
			s = base64_encode(s)
				;
			s.Insert(3, SALT6);
			s.Insert(4, SALT5);
			s = base64_encode(s);
			return s;
		}

		public static string DecryptB64(string s)
		{
            return base64_decode(s); // make work without this
            s = base64_decode(s);   // s = base64_encode(s); 
			s = TakeOutIndex(s, 4, SALT5);  // s.insert(4, SALT5);
			s = TakeOutIndex(s, 3, SALT6);  // s.insert(3, SALT6);
			s = base64_decode(s);   // s = base64_encode(s);
			s = TakeOutEnd(s, SALT1); s = TakeOutEnd(s, SALT3); s = TakeOutStart(s, SALT2);   // s = SALT2 + s + SALT3 + SALT1;
			s = base64_decode(s);   // s = base64_encode(s); 
			s = TakeOutEnd(s, SALT3); s = TakeOutStart(s, SALT1); s = TakeOutStart(s, SALT2);   // s = SALT1 + SALT2 + s + SALT3;
			s = TakeOutIndex(s, 13, SALT5); // s.insert(13, SALT5);	
			s = TakeOutIndex(s, 25, SALT4); // s.insert(25, SALT4);
			s = base64_decode(s);   // s = base64_encode(s); 
			s = TakeOutEnd(s, SALT3); s = TakeOutEnd(s, SALT2); s = TakeOutStart(s, SALT1);   // s = SALT1 + s + SALT2 + SALT3;
			return s;
		}

		public static string base64_encode(string s)
		{
            string base64Encoded;
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(s);
            base64Encoded = System.Convert.ToBase64String(data);
            return base64Encoded;
		}

		public static string base64_decode(string s)
		{
            var encodedTextBytes = Convert.FromBase64String(s);
            string plainText = Encoding.UTF8.GetString(encodedTextBytes);
            return plainText;
		}

		public static string TakeOutEnd(string s, string what)
		{
			if (string.IsNullOrEmpty(what))
				return s;

			int lenNumberWhat = what.Length;
			int lenNumberS = s.Length;

			string firstPart = (s.Substring(0, lenNumberS - lenNumberWhat));;

			return firstPart;
		}

		public static string TakeOutStart(string s, string what)
		{
			if (string.IsNullOrEmpty(what))
				return s;

			int lenNumber = (int)what.Length;

			string lastPart = (s.Substring(lenNumber));

			return lastPart;
		}

		public static string TakeOutIndex(string s, int index, string what = "X")
		{
			if (index < 1)
			{
				return TakeOutStart(s, what);
			}

			int lenNumber = (int)what.Length;

			string firstPart = (s.Substring(0, index)); ;
			string lastPart = (s.Substring(index + lenNumber));

			return firstPart + lastPart;
		}


	}
}
