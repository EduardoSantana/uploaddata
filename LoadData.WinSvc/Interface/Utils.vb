﻿Imports System.Reflection
Imports System.Configuration

Public Class Utils

	Public Shared Function GetConfigurationValue(key As String) As String
		Dim service As Assembly = Assembly.GetAssembly(GetType(TCW_Service_V20))
		Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(service.Location)
		If config.AppSettings.Settings(key) IsNot Nothing Then
			Return config.AppSettings.Settings(key).Value
		Else
			Throw New IndexOutOfRangeException(Convert.ToString("Settings collection does not contain the requested key: ") & key)
		End If
	End Function

End Class
