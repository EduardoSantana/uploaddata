﻿Public MustInherit Class clProgramaServicio : Implements IclProgramaServicio

#Region "Variables"
	Public Property _EventLog1 As EventLog Implements IclProgramaServicio._EventLog1
	Public Property _timerUpload As Timers.Timer Implements IclProgramaServicio._timerUpload
	Public Property _trdUploadNotificacion As Threading.Thread Implements IclProgramaServicio._trdUploadNotificacion
	Public ReadOnly Property _TiempoDeNotificacionUpload As Integer Implements IclProgramaServicio._TiempoDeNotificacionUpload
		Get
			Return CInt(Configuration.ConfigurationManager.AppSettings("TiempoDeNotificacion")) * 1000
		End Get
	End Property
#End Region

#Region "Eventos"

	Public Sub _timerUpload_Tick(ByRef even As EventLog) Implements IclProgramaServicio._timerUpload_Tick
		_EventLog1 = even
		If _trdUploadNotificacion Is Nothing OrElse Not _trdUploadNotificacion.IsAlive Then
			_trdUploadNotificacion = New Threading.Thread(AddressOf Me._timerUpload_Tick_Hijo)
			_trdUploadNotificacion.IsBackground = True
			_trdUploadNotificacion.Start()
		End If
	End Sub

	Public Sub _timerUpload_Tick_Hijo() Implements IclProgramaServicio._timerUpload_Tick_Hijo

		Try

			Dim strOrdenesNoProcesadas As String = Me.BeginUpload()

			If strOrdenesNoProcesadas.ToString <> "" Then

				If strOrdenesNoProcesadas.Length > 2000 Then
					strOrdenesNoProcesadas = strOrdenesNoProcesadas.Substring(0, 2000)
				End If
				If _EventLog1 IsNot Nothing Then
					_EventLog1.WriteEntry(strOrdenesNoProcesadas, EventLogEntryType.Warning, 2)
				End If

			End If
		Catch ex As Exception

			If _EventLog1 IsNot Nothing Then
				_EventLog1.WriteEntry(ex.Message, EventLogEntryType.FailureAudit, 3)
			Else
				Exit Sub
				'Throw New Exception(ex.Message)
			End If

		End Try

	End Sub

	Public MustOverride Function BeginUpload() As String Implements IclProgramaServicio.BeginUpload

#End Region

End Class
