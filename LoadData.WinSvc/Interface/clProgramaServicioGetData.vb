﻿Imports LoadData.LoadData
Class clProgramaServicioGetData
	Inherits clProgramaServicio
	Public Overrides Function BeginUpload() As String
		' Llama al método de la Clase Base y retorna el Valor
		Dim retVal As String = ""
		Try
			Dim _programa = New LogicaPrograma()
			_programa.HiloCentralRobarLasPeliculasConsole(True, Me._EventLog1)
			_programa.HiloCentralRobarLasPeliculasConsole(False, Me._EventLog1)
			_programa.CargarPeliculasAlWordPressOnline(Me._EventLog1)
		Catch ex As Exception
			retVal = "Mensaje: " + ex.Message + vbNewLine + " | " + vbNewLine + _
					 "Innder Execption: " + ex.InnerException.ToString + vbNewLine + " | " + vbNewLine
		End Try

		Return retVal
	End Function
End Class
