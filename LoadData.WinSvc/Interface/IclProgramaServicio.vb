﻿Public Interface IclProgramaServicio

#Region "Variables"
	Property _EventLog1 As EventLog
	Property _timerUpload As Timers.Timer
	Property _trdUploadNotificacion As Threading.Thread
	ReadOnly Property _TiempoDeNotificacionUpload As Integer
#End Region

#Region "Eventos"
	Sub _timerUpload_Tick(ByRef even As EventLog)
	Sub _timerUpload_Tick_Hijo()
	Function BeginUpload() As String
#End Region

End Interface
