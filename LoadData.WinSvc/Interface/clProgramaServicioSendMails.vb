﻿Imports EasyLoan.CorreosManager
Class clProgramaServicioSendMails
	Inherits clProgramaServicio
	Public Overrides Function BeginUpload() As String
		' Llama al método de la Clase Base y retorna el Valor
		Dim retVal As String = ""
		Try
			MailsManager.Enviar()
		Catch ex As Exception
			retVal = "Mensaje: " + ex.Message + vbNewLine + " | " + vbNewLine + _
					 "Innder Execption: " + ex.InnerException.ToString + vbNewLine + " | " + vbNewLine
		End Try

		Return retVal
	End Function
End Class
