﻿Imports System.Configuration

Partial Class TCW_Service_V20

	'Private Sub InitializeComponent()
	'	Me.ServiceProcessInstaller1 = New System.ServiceProcess.ServiceProcessInstaller()
	'	Me.ServiceInstaller1 = New System.ServiceProcess.ServiceInstaller()
	'	'
	'	'ServiceProcessInstaller1
	'	'
	'	Me.ServiceProcessInstaller1.Password = Nothing
	'	Me.ServiceProcessInstaller1.Username = Nothing
	'	'
	'	'ServiceInstaller1
	'	'
	'	Me.ServiceInstaller1.Description = "Sincroniza la tabla de actividades de ST con la tabla intermedia SAV DEV IN del o" & _
	'"perador."
	'	Me.ServiceInstaller1.DisplayName = "TCW - Modulo Notificar V20 Orange"
	'	Me.ServiceInstaller1.ServiceName = "TCW - Modulo Notificar V20 Orange"
	'	Me.ServiceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic
	'	'
	'	'TCW_Service_V20
	'	'
	'	Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.ServiceProcessInstaller1, Me.ServiceInstaller1})

	'End Sub

	Public ReadOnly Property ServiceName() As String
		Get
			Dim retVal As String = Utils.GetConfigurationValue("ServiceName")
			If retVal.Trim = "" Then
				Throw New Exception("No tiene el nombre del ServiceName.")
			End If
			Return retVal
		End Get
	End Property
	Public ReadOnly Property DisplayName() As String
		Get
			Dim retVal As String = Utils.GetConfigurationValue("DisplayName")
			If retVal.Trim = "" Then
				Throw New Exception("No tiene el nombre del DisplayName.")
			End If
			Return retVal
		End Get
	End Property
	Public ReadOnly Property Description() As String
		Get
			Dim retVal As String = Utils.GetConfigurationValue("Description")
			If retVal.Trim = "" Then
				Throw New Exception("No tiene el nombre del Description.")
			End If
			Return retVal
		End Get
	End Property

End Class

