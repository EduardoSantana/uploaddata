﻿Imports System.ComponentModel
Imports System.Configuration.Install

Public Class TCW_Service_V20

	Public Sub New()
		MyBase.New()

		'This call is required by the Component Designer.
		InitializeComponent()

		'Add initialization code after the call to InitializeComponent
		Me.ServiceInstaller1.Description = Me.Description
		Me.ServiceInstaller1.DisplayName = Me.DisplayName
		Me.ServiceInstaller1.ServiceName = Me.ServiceName

	End Sub

End Class
