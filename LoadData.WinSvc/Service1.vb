﻿Imports System.Configuration

Public Class Service1

	Dim _upload As IclProgramaServicio = New clProgramaServicioSendMails()

	Protected Overrides Sub OnStart(ByVal args() As String)
		' Add code here to start your service. This method should set things
		' in motion so your service can do its work.

		_upload._EventLog1 = EventLog1

		EventLog1.WriteEntry("In OnStart", EventLogEntryType.Information, 1)

		Try

			_upload._timerUpload = New System.Timers.Timer(_upload._TiempoDeNotificacionUpload)
			AddHandler _upload._timerUpload.Elapsed, (Sub()
														  _upload._timerUpload_Tick(EventLog1)
													  End Sub)

			With _upload._timerUpload
				.AutoReset = True
				.Enabled = True
				.Start()
			End With

			GC.KeepAlive(_upload._timerUpload)
		Catch ex As Exception
			EventLog1.WriteEntry(ex.Message, EventLogEntryType.FailureAudit, 1)
		End Try
	End Sub

	Protected Overrides Sub OnStop()
		' Add code here to perform any tear-down necessary to stop your service.
		Try
			If _upload._timerUpload IsNot Nothing Then
				_upload._timerUpload = Nothing
			End If
		Catch ex As Exception
			EventLog1.WriteEntry(ex.Message, EventLogEntryType.Error, 1)
		End Try
		EventLog1.WriteEntry("In OnStop.", EventLogEntryType.Information, 1)

	End Sub

	''' <summary>
	''' Inicializa el Even Log
	''' </summary>
	''' <remarks></remarks>
	Public Sub New()
		MyBase.New()
		' This call is required by the designer.
		InitializeComponent()

		Me.ServiceName = ConfigurationManager.AppSettings.Get("ServiceName")

		Dim EventLog_Source As String = Me.EventLog_Source
		'Dim EventLog_Log_Name As String = Me.EventLog_Source
		If Not System.Diagnostics.EventLog.SourceExists(EventLog_Source) Then
			System.Diagnostics.EventLog.CreateEventSource(EventLog_Source, EventLog_Source)
		End If
		EventLog1.Source = EventLog_Source
		EventLog1.Log = EventLog_Source

	End Sub

	Public ReadOnly Property EventLog_Source() As String
		Get
			Dim retVal As String = Utils.GetConfigurationValue("EventLog_Source")
			If retVal.Trim = "" Then
				Throw New Exception("No tiene el nombre del EventLog_Source.")
			End If
			Return retVal
		End Get
	End Property

End Class
